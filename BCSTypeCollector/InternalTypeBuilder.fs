﻿module InternalTypeBuilder

open LuaAnalyzer.Type
open LuaAnalyzer.TypeCollector
open LuaAnalyzer.Context
open LuaAnalyzer.Analyzer
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.TypeChecker
open LuaAnalyzer.ProjectChecker
open LuaAnalyzer.Analyzer
open LuaAnalyzer.Utils

let extractPrivateFields (parentPrivFields : FieldMap) (exTy : Type) (fileName : string) (ctxt : Context) (consBody : TypedStatement) : Map<string,Field> =
    
    let newBody = new GenList<TypedStatement>([])
    newBody.Add(consBody)
    newBody.Add(Call(CallExpr(NameExpr("lucbdnioua",(0,0)), [], (0,0)), (0,0)))

    // append a call to lucbdnioua onto consBody
    let consBody = Sequence(None,newBody, (0,0))
 
    let exMeta,exFields,exMethods =
        match exTy.StripOuterAbstractions() with
        | RecordTy(_,_,_,_,_,meta,fields,[],methods,[],_) ->
            meta,fields,methods
        | _ ->
            failwith "unreachable"

    // extract the type of self at that point using the typechecker
    let selfType = 
            RecordTy(
                "*PartiallyConstructedObject*",
                "",
                true,
                false,
                Open,
                exMeta,
                cover parentPrivFields exFields,
                [],
                exMethods,
                [],
                NoLocation
            )
    
    let selfField = {
        desc = "the object being constructed"
        ty = selfType
        dependencies = Set.empty
        loc = (fileName,(0,0))
        isVisible = true
        isConst = true
    }

    let ctxt = getStubContext (ctxt.AddValue("self",selfField)) consBody fileName
    match Type.Coerce ctxt.tenv (ctxt.venv.Item "self").ty with
    | RecordTy(_,_,_,_,Open,_,fields,_,_,_,_) ->
        fields
    | _ ->
        Map.empty

let internalTypeBuilder (globalCtxt : Context) 
                        (modname : string) 
                        (fileName : string)
                        (ast : TypedStatement) =
    
    flushErrors()

    let venv, tenv = globalCtxt.venv, globalCtxt.tenv
    let typeMap = tenv.typeMap

    let className,parentName, rng =
        match getName "" ast with
        | Some(className,parentName,rng) ->
            className,parentName, rng
        | None ->
            failwith "unreachable"

    let parentPrivFields, parentMethods =
        match parentName with
        | None ->
            Map.empty, Map.empty
        | Some parentName ->
            match typeMap.Item (parentName + "|private") with
            | RecordTy(_,_,_,_,Closed,_,fields,[],methods,[],_) ->
                fields, methods
            | _ ->
                failwith "unreachable"

    //TODO: we should yoink these from the external type rather than re-extracting them
    let _,events,_ = getMethods parentMethods fileName modname className ast false

    let makeEventTriggerer (name : string) (event : Event) =
        let desc = event.desc
        let ty = event.ty
        let loc = event.loc

        match ty with
        | FunctionTy(desc,pars,varPars,rets,varRets,_,false,loc) ->
            let methTy = FunctionTy(desc,pars,varPars,rets,varRets,true,false,loc)
            {desc=desc;ty=methTy;isAbstract=true;loc=loc}
        | UnknownTy ->
            {
                desc = "";
                ty = FunctionTy(desc,[],Some(""),[],Some(""),true,false,loc)
                isAbstract=true
                loc = loc
            }
        | _ ->
            failwith "events should have non-latent function types"

    let eventMethods = Map.map makeEventTriggerer events

    match ast with
    | MethodDefInCtxt globalCtxt "init" (Some (body,ctxt,_)) ->
        let exTy = (typeMap.Item modname).StripOuterAbstractions()
        let privateFields = extractPrivateFields parentPrivFields exTy fileName ctxt body
        let nonAncestorDependencies = Set.unionMany (List.map (fun ((_,f) : string*Field) -> f.dependencies) (Map.toList privateFields))
        match exTy with
        | RecordTy(name,desc,isStructural,displayStructural,Closed,metaset,publicFields,[],publicMethods,[],defLoc) ->
            let fields = cover parentPrivFields privateFields 
            let fields = cover fields publicFields
            let methods = cover parentMethods publicMethods 
            let methods = cover methods eventMethods
            //TODO: must be mutually exclusive
            new Map<string,Type>([(modname + "|private",RecordTy(modname + "|private",desc,isStructural,displayStructural,Closed,metaset,fields,[],methods,[],(fileName,rng)))]),
            !errors,
            List.ofSeq nonAncestorDependencies
        | _ ->
            failwith "unreachable"
    | _ ->
        new Map<string,Type>([(modname + "|private",RecordTy(modname + "|private","",false,false,Closed,MetamethodSet.empty,Map.empty,[],Map.empty,[],(fileName,rng)))]),
        !errors,
        []
