﻿[<AutoOpen>]
module Utils

open LuaAnalyzer.Type
open LuaAnalyzer.Utils
open LuaAnalyzer.Syntax
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.Operators
open LuaAnalyzer.Annotations

type Event = Field
type FieldMap = Map<string, Field>
type MethodMap = Map<string, Method>
type EventMap = Map<string, Event>

let errors : Ref< List<Error> > = ref []

let flushErrors () =
    errors := []

let addError fileName msg rng =
    errors := (fileName,msg,rng) :: !errors

/// True iff ast ends with the statement "return *name*"
let rec returnsName (name : string) (ast : TypedStatement) =
    match ast with
    | EndsWith (Return([NameExpr(x,_)],_)) when x = name ->
        true
    | _ ->
        false

/// methodName - the name of the defined method
/// methodTy - the type of the defined method
/// desc - a description of the defined method
/// fileName - the name of the file containing the definition
/// nameRng - the range in char indices which the method name occurs in
///
/// From info extracted from a operator definition (operator overloads
/// in addition to public methods are considered methods for these purposes),
/// returns a metamethodSet containing the contribution this operator makes to the 
/// metamethod set of the class being defined.
let getOperator (methodName : string) 
                (methodTy : Type) 
                (desc : string) 
                (fileName : string) 
                (nameRng : Range) =

    let defLocation = (fileName,nameRng)
    
    match methodName with
    | "__add" ->
        let desc, rhsTy, retTy =
            match methodTy with
            | FunctionTy(desc,[(_,_,rhsTy)],None, [(_,retTy)],None,_,false,_) ->
                desc, rhsTy, retTy
            | _ ->
                "", UnknownTy, UnknownTy
        { MetamethodSet.empty with
            Add = Some(desc,rhsTy,retTy,defLocation)
        }
    | "__sub" ->
        let desc, rhsTy, retTy =
            match methodTy with
            | FunctionTy(desc,[(_,_,rhsTy)],None, [(_,retTy)],None,_,false,_) ->
                desc, rhsTy, retTy
            | _ ->
                "", UnknownTy, UnknownTy
        { MetamethodSet.empty with
            Sub = Some(desc,rhsTy,retTy,defLocation)
        }
    | "__mul" ->
        let desc, rhsTy, retTy =
            match methodTy with
            | FunctionTy(desc,[(_,_,rhsTy)],None, [(_,retTy)],None,_,false,_) ->
                desc, rhsTy, retTy
            | _ ->
                "", UnknownTy, UnknownTy
        { MetamethodSet.empty with
            Mul = Some(desc,rhsTy,retTy,defLocation)
        }
    | "__div" ->
        let desc, rhsTy, retTy =
            match methodTy with
            | FunctionTy(desc,[(_,_,rhsTy)],None,[(_,retTy)],None,_,false,_) ->
                desc, rhsTy, retTy
            | _ ->
                "", UnknownTy, UnknownTy
        { MetamethodSet.empty with
            Div = Some(desc,rhsTy,retTy,defLocation)
        }
    | "__unm" ->
        let desc, retTy =
            match methodTy with
            | FunctionTy(desc,[],None,[(_,retTy)],None,_,false,_) ->
                desc, retTy
            | _ ->
                "", UnknownTy

        { MetamethodSet.empty with
            Unm = Some(desc,retTy,defLocation)
        }
    | _ ->
        addError 
            fileName 
            ("metamethod " + methodName + " not recognized by basic class system")
            nameRng
        MetamethodSet.empty

/// parentMethods - the method map of the parent class; we use this to provide
///   parameter and return types when none are given in the child definition
/// fileName - the name of the file we are extracting method type signatures from
/// moduleName - the name of the module we are extracting method type signatures from
/// className - the name of the class whose definition we are extracting method type signatures from
/// ast - the syntax tree for the file we are extracting from
/// addErrors - whether or not to add errors if any are encountered
///
/// extracts all method definitions ('methods' loosely refers to operators, events, 
/// abstract methods, and concrete methods in this case) from the specified file.
let rec getMethods (parentMethods : MethodMap) 
                   (fileName : string) 
                   (moduleName : string) 
                   (className : string) 
                   (ast : TypedStatement) 
                   (addErrors : bool)
                   : MethodMap*EventMap*MetamethodSet =

    match ast with
    | Assign(
        annotation,
        [BinOpExpr(OpSelect, BinOpExpr(OpSelect, NameExpr(x,_), String("events",_),_),String(methName,nameRng),_)], 
        [Constructor([],_)],
        _
      ) when x = className ->

        // This must be an event declaration
        let desc,methodTy =
            match Type.InterpretFunctionAnnotation annotation with
            | Some(desc,formals,varPars,rets,varRets,latent) -> 
                desc,
                FunctionTy("",formals,varPars,rets,varRets,false,latent,(fileName, nameRng))
            | None ->
                //TODO: generate an error message
                "",UnknownTy

        let event = {
            isConst = true
            isVisible = true
            desc = desc
            ty = methodTy
            dependencies = Set.empty
            loc = (fileName,nameRng)
        }


        Map.empty,
        Map.add methName event Map.empty,
        MetamethodSet.empty

    | Assign(
        annotation,
        [BinOpExpr(OpSelect,NameExpr(x,_),String(methName,nameRng),_)], 
        [Constructor([],_)],
        _
      ) when x = className ->
        
        // This must be an abstract method declaration

        let desc,methodTy =
            match Type.InterpretFunctionAnnotation annotation with
            | Some(desc,formals,varPars,rets,varRets,latent) -> 
                desc,
                FunctionTy(desc,formals,varPars,rets,varRets,true,latent,(fileName, nameRng))
            | None ->
                "",UnknownTy

        new MethodMap([(methName,{desc=desc;ty=methodTy;isAbstract=true;loc=(fileName,nameRng)})]), 
        Map.empty,
        MetamethodSet.empty

    | Assign(
        _,
        [BinOpExpr(OpSelect,NameExpr(x,_),String(methName,nameRng),_)], 
        [Function(ann,selfName,latent,self :: formals,varPars,rets,varRets,body,(startLoc,_))],
        _
      ) when (x = className && methName.StartsWith("__")) ->
        let desc = if ann.IsSome then fst4 ann.Value else ""
        let methodTy = FunctionTy(desc,formals,varPars,rets,varRets,true,latent,(fileName, nameRng))
        Map.empty,
        Map.empty,
        getOperator methName methodTy desc fileName nameRng
    | Assign(
        _,
        [BinOpExpr(OpSelect,NameExpr(x,_),String(methName,nameRng),_)], 
        [Function(ann,selfName,latent,self :: formals,varPars,rets,varRets,body,(startLoc,_))],
        _
      ) when x = className ->
        let desc = if ann.IsSome then fst4 ann.Value else ""
        let formals,rets,latent = 
          match parentMethods.TryFind methName with
          | Some{desc=_;ty=FunctionTy(_,pformals,varPars,prets,varRets,_,platent,_);isAbstract=_;loc=_} ->
            let mapFormal (name : string ,desc : string, ty : Type) =
                 match List.tryFind (fun (n,d,t) -> n = name) pformals with
                 | Some(pname,pdesc,pty) when ty = UnknownTy ->
                    (pname,pdesc,pty)
                 | _ ->
                    (name, desc, ty)

            let prets = sizeListTo prets rets.Length ("",UnknownTy)

            let mapRet (ret : string*Type) (pret : string*Type) =
                match ret with
                | (_, UnknownTy) ->
                    pret
                | _ ->
                    ret

            List.map mapFormal formals, List.map2 mapRet rets prets, platent || latent
          | _ ->
            formals,rets,latent

        let methodTy = FunctionTy(desc,formals,varPars,rets,varRets,true,latent,(fileName, nameRng))
        let meth = {
            desc = desc
            ty = methodTy
            isAbstract = false
            loc = (fileName,nameRng)
        }

        new MethodMap([(methName,meth)]), 
        Map.empty,
        MetamethodSet.empty

    | Sequence(_,stats,_) ->
        let methodError (name : string) (meth : Method) =
            if addErrors then
                addError
                    fileName
                    (name + " already defined.")
                    (snd meth.loc)

        let eventError (name : string) (event : Event) =
            if addErrors then
                addError
                    fileName
                    (name + " already defined.")
                    (snd event.loc)

        let metaMethodError (name : string) (loc : DefinitionLocation) =
            if addErrors then
                addError
                    fileName
                    (name + " already defined.")
                    (snd loc)

        let foldStat (methods,events,meta) (stat : TypedStatement) =
            let statMethods,statEvents,statMeta =
                getMethods parentMethods 
                           fileName 
                           moduleName 
                           className 
                           stat
                           addErrors
            
            weave methods statMethods methodError, 
            weave events statEvents eventError, 
            MetamethodSet.weave meta statMeta metaMethodError

        Seq.fold foldStat (Map.empty,Map.empty,MetamethodSet.empty) stats 
    | _ ->
        Map.empty, 
        Map.empty,
        MetamethodSet.empty

/// currentFile - the full path of the file being searched
/// ast - the syntax of the file being searched
///
/// If the specified file contains a basic class system class definition,
/// we return the (name,optParentName,rng) triple, where name is the name
/// of the class being defined, optParentName is the name of the parent class
/// of the class being defined (or none if the defined class does not inherit),
/// and rng is the range in character indices in which the class name appears.
let rec getName (currentFile : string) (ast : TypedStatement) =
    let rec getNameRec (ast : TypedStatement) (ctxt : Map<string,string>) =
        match ast with
        | LocalAssign(
            _,
            [NameExpr(name,rng)],
            [CallExpr(
                BinOpExpr(OpMethInd,NameExpr("class",prng),String("inherit",_),_),
                [NameExpr(parentName,_)],
                _
            )],
            _
          ) ->
            if ctxt.ContainsKey parentName then
                Some (name, Some(ctxt.Item parentName), rng)
            else
                addError currentFile (parentName + " not a class.") prng
                None
        | LocalAssign(
            _,
            [NameExpr(name,rng)],
            [CallExpr(
                BinOpExpr(OpMethInd,NameExpr("class",prng),String("new",_),_),
                _,
                _
            )],
            _
          ) ->
            Some (name,None,rng)
        | Assign(
            _,
            [NameExpr(name,rng)],
            [CallExpr(
                BinOpExpr(OpMethInd,NameExpr(_,_),String("new",_),_),
                _,
                _
            )],
            _
            ) ->

            addError currentFile "Global style module definitions not allowed." rng
            None
        | Sequence(_,stats,_) when stats.Count > 0 ->
          let ctxt = ref ctxt
          let res = ref None
          for s in stats do
            if (!res).IsNone then
                let n = getNameRec s (!ctxt)
                if n.IsSome then
                    res := n

            ctxt :=
                match s with
                | LocalAssign(
                    _,
                    [NameExpr(name,rng)],
                    [CallExpr(NameExpr("require",_),String(modName,_) :: _,_)],
                    _
                    ) ->
                    (!ctxt).Add(name,modName)
                | LocalAssign(_,[NameExpr(name,_)],_,_)
                | Assign(_,[NameExpr(name,_)],_,_) ->
                    (!ctxt).Remove(name)
                | _ ->
                    !ctxt               
          !res
        | _ ->
            None 
    getNameRec ast Map.empty
