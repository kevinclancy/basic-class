﻿module ExternalTypeBuilder

open LuaAnalyzer.Type
open LuaAnalyzer.Utils
open LuaAnalyzer.Syntax
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.SubtypeGraphBuilder

open TypeGraphBuilder // getName

/// events - a map containing all events declared by the class
///
/// returns a map containing, for each event, a record with subscribe and unsubscribe
/// methods for that event.
let makeEvents (events : EventMap) : FieldMap =

    let makeExternalEventInterface (eventName : string) (event : Event) =
        let {desc=desc;ty=ty;loc=loc;isConst=_;isVisible=_} = event

        // The type of the subscribe function: takes a subscriber and returns nothing
        let subscribeTy = FunctionTy(
            "", 
            [("subscriber","",ty)],
            None,
            [],
            None,
            true,
            false,
            loc
        )

        let unsubscribeTy = FunctionTy(
            "",
            [("unsubscriber","",ty)],
            None,
            [],
            None,
            true,
            false,
            loc
        )

        let eventInterfaceMethods = Map.empty.Add(
            "subscribe",
            {
                desc = "subscribe to this event"
                ty = subscribeTy;
                isAbstract=false
                loc = loc
            }
        )
        
        let eventInterfaceMethods = eventInterfaceMethods.Add(
            "unsubscribe",
            {
                desc="unsubscribe from this event"
                ty=unsubscribeTy
                isAbstract=false
                loc = loc
            }
        )

        {
            desc = desc
            ty = RecordTy(
                eventName + "|interface",
                desc,
                true,
                false,
                Closed,
                MetamethodSet.empty,
                Map.empty,
                [],
                eventInterfaceMethods,
                [],
                loc
            )
            dependencies = Set.empty
            loc = loc
            isVisible = true
            isConst = true
        }


    Map.map makeExternalEventInterface events

/// className - the name of the class we are constructing an events record for
/// classLoc - the location of the definition for the class we are constructing an events record for
/// events - a map containing all events declared by the class
///
/// creates an event interface for a class instance (accessed via instance.events), 
/// this is a record containing one field for each event. Each field is named after an 
/// event and contains two methods: subscribe and unsubscribe methods for that event.
let makeEventsRecord (className : string) 
                     (classLoc : DefinitionLocation) 
                     (events : FieldMap) =

    RecordTy(
        className + "|events",
        "subscribe interfaces for all of " + className + "'s events",
        true,
        false,
        Closed,
        MetamethodSet.empty,
        events,
        [],
        Map.empty,
        [],
        classLoc
    )


/// Given a key and a method in a method map, return Some key if the
/// method is abstract and None if it is concrete.
let pickIfAbstract (key : string) (meth : Method) : Option<string> =
    match meth.isAbstract with
    | true ->
        Some key
    | false ->
        None

/// methods - the method map of the class we are creating a delegates record type for
/// path - the path of the file which defines the class
/// rng - the range in character indices in which class name occurs in the file specified by path 
///
/// Each object has a field called delegates, which refers to a table
/// containing one field for each of the object's methods. Each field
/// is the delegate for its corresponding method; i.e. it is a loose
/// function equivalent to the corresponding method, except with the
/// self argument bound to the object.
let makeDelegatesRecord (methods : MethodMap) (path : string) (rng : Range) =    
    let makeDelegate (key : string) (meth : Method) : Field =
        let ty =
            match meth.ty with
            | FunctionTy(desc,pars,varPars,rets,varRets,isMethod,latent,defLoc) ->
                FunctionTy(desc,pars,varPars,rets,varRets,false,latent,defLoc)
            | UnknownTy ->
                UnknownTy
            | _ ->
                failwith "unreachable"

        {
            desc = ""
            ty = ty
            dependencies = Set.empty
            isConst = true
            isVisible = true
            loc = meth.loc
        }

    let isNormalMethod (name : string) (meth : Method) =
        not (name = "init") && not (name.Substring(0,2) = "__")
            
    RecordTy(
        "|DelegateType",
        "",
        false,
        false,
        Closed,
        MetamethodSet.empty,
        Map.map makeDelegate (Map.filter isNormalMethod methods),
        [],
        Map.empty,
        [],
        (path,rng)
    )

/// modname - the name of the module defining the class that we are creating an external type for
/// path - the path of the file defining the class that we are creating an external type for
/// rng - the range in which the class name occurs in the file specified by pathname
/// events - the set of events defined by the class being processed
/// desc - a description of the external type for intellisense
/// methods - the set of methods defined by the class being processed
/// meta - the metamethod set for the class being processed
/// returns - an (instanceType, constructorType) pair for the class
let makeExternalType (modname : string) 
                     (path : string) 
                     (rng : Range) 
                     (desc : string)
                     (eventInterfaces : FieldMap)
                     (methods : MethodMap)
                     (meta : MetamethodSet)
                     (typeVars : List<string*string*DefinitionLocation>) =
    
    let events = makeEventsRecord modname (path,rng) eventInterfaces
    let eventsField = {
        desc = "events"
        ty = events
        dependencies = Set.empty
        loc = (path,rng)
        isConst = true
        isVisible = true
    }
            
    let delegatesTy = makeDelegatesRecord methods path rng
    let delegateField = {
        desc = "delegates for this object's methods"
        ty = delegatesTy
        dependencies = Set.empty
        loc = (path,rng)
        isConst = true
        isVisible = true
    }

    let classFields = Map.add "events" eventsField Map.empty
    let classFields = Map.add "delegates" delegateField classFields
    let desc = if desc = "" then "No description given for " + modname + "." else desc
    let classTy = RecordTy(modname,desc,false,false,Closed,meta,classFields,[],methods.Remove("init"),[],(path,rng))
    let absClassTy = if typeVars.Length > 0 then TypeAbsTy(List.map (fun (nm,desc,loc) -> (nm,desc)) typeVars, classTy) else classTy
    let cons =
        match methods.Item "init" with
        | {desc=_;
            ty=FunctionTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation);
            isAbstract=false;
            loc=loc
            } ->
            let optAbsMethodName = Map.tryPick pickIfAbstract methods 
                      
            let consMeta = 
                    { 
                    MetamethodSet.empty with
                        Call = 
                            match optAbsMethodName with
                            | None ->
                                Some("",formals,varPars,[("new instance of " + modname + " class", absClassTy)],None,false,false,loc)
                            | Some key ->
                                let msg = "Cannot instantiate. Class " + modname + " is abstract because method " + key + " has not been implemented."
                                Some("",formals,varPars,[("Do not call." + msg, ErrorTy(msg))],None,false,false,loc)
                    }
            let newMethod = {
                desc = "define a new class which inherits from this one";
                ty = UnknownTy
                isAbstract=false
                loc = loc
            }
            let consMethods = Map.empty.Add("new",newMethod)
            RecordTy(modname+"|constructor","",true,false,Closed,consMeta,Map.empty,[],consMethods,[],loc)
        | {desc=_;
            ty=_;
            isAbstract=_;
            loc=loc
            }  ->

            addError
                path
                "constructors should always be concrete methods"
                (snd loc)
            UnknownTy

    absClassTy, cons

let externalTypeBuilder (env : TypeEnvironment) (modname : string) (path : string) (ast : TypedStatement) =
    flushErrors()

    let typeMap = env.typeMap
    match getName path ast with
    | Some (name,parentName,rng) when (returnsName name ast) ->
        let parentMeta, parentFields, parentMethods, parentHasTypeArgs =
            match parentName with
            | Some parentName when typeMap.ContainsKey parentName->
                match typeMap.Item (parentName) with
                | RecordTy(_,_,_,_,Closed,parentMeta,parentFields,[],parentMethods,_,_) ->
                    parentMeta, parentFields, parentMethods, false
                | UnknownTy ->
                    addError
                        path
                        ("specified parent " + parentName + " is not a class")
                        rng
                    MetamethodSet.empty, Map.empty, Map.empty, false
                | TypeAbsTy(varInfo,body) ->
                    addError
                        path
                        ("cannot inherit from " + parentName + ". Inheritence from classes with type arguments is not allowed.")
                        rng
                    MetamethodSet.empty,Map.empty,Map.empty, true
                | _ ->
                    failwith "type collector error: module types should only be records"
            | _ ->
                MetamethodSet.empty, Map.empty, Map.empty, false
        let parentEvents = 
            if parentFields.Count > 0 then
                match parentFields.["events"].ty with
                | RecordTy(_,_,_,_,_,_,events,_,_,_,_) ->
                    events
                | _ ->
                    failwith "unreachable"
            else
                Map.empty

        let description = 
            match ast with
            | Sequence(Some(desc,_,_,_),_,_) ->
                desc
            | _ ->
                ""

        let methods,events,meta = getMethods parentMethods path modname name ast true
        let methods = cover parentMethods methods
        let events = cover parentEvents (makeEvents events)
        let meta = MetamethodSet.cover parentMeta meta
        let meta = MetamethodSet.cover MetamethodSet.standard meta
        
        if methods.ContainsKey "init" then
            let classTy, cons = makeExternalType modname
                                                 path
                                                 rng
                                                 description
                                                 events
                                                 methods
                                                 meta
                                                 (getTypeVars ast)
            Some (classTy, cons), !errors
        else
            addError path "no constructor defined" rng
            None, !errors
    | _ ->
        //TODO: is this even reachable?
        raise( CollectorError "could not build external type" )
