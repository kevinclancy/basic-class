﻿module TypeGraphBuilder

open LuaAnalyzer.Syntax
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.Operators

// TODO: user active pattern here
let rec hasInit (name : string) (ast : TypedStatement) =
    match ast with
    |  Assign(
        annotation,
        [BinOpExpr(OpSelect,NameExpr(x,_),String("init",_),_)], 
        [Function(_,_,_,self :: _,_,_,_,_,(_,_))],
        _
      ) when x = name ->
        true
    | Sequence(_,stats,_) ->
        Seq.exists (fun s -> hasInit name s) stats
    | _ ->
        false

let typeGraphBuilder (modname : string) (fileName : string) (ast : TypedStatement) =
    flushErrors()

    match getName fileName ast with
    | Some (name,parentName, rng) when (returnsName name ast) ->
        let internTypes = [(modname + "|private",rng)]

        if not (hasInit name ast) then
            addError 
                fileName
                "No init method defined"
                rng
            None, !errors
        elif List.isEmpty (getTypeVars ast) then
            let edges = 
                match parentName with
                | None ->
                    [(modname + "|private", modname)]
                | Some parentName ->
                    [
                        (modname + "|private", modname)
                        (modname,parentName)
                    ]
            
            Some (rng,internTypes,edges), !errors
        else
            let edges = 
                match parentName with
                | None ->
                    []
                | Some parentName ->
                    [
                        (modname,parentName)
                    ]

            Some (rng,internTypes,edges), !errors
            
    | _ ->
        None, !errors


