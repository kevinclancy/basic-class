﻿module Main

open LuaAnalyzer.Type
open LuaAnalyzer.Syntax
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.TypeChecker
open LuaAnalyzer.ErrorList
open LuaAnalyzer.Utils
open LuaAnalyzer.ErrorList
open LuaAnalyzer.Analyzer
open LuaAnalyzer.Context
open LuaAnalyzer.SubtypeGraphBuilder
open LuaAnalyzer.TypeCollector
open LuaAnalyzer.ProjectChecker
open LuaAnalyzer.Plugins
open LuaAnalyzer.Operators

open System.ComponentModel.Composition

open TypeGraphBuilder
open ExternalTypeBuilder
open InternalTypeBuilder

let detectMiscErrors (ctxt : Context) (modulePath : string) (filePath : string) (ast : TypedStatement) =
    let venv, tenv = ctxt.venv, ctxt.tenv
    if (tenv.edges.Item modulePath).Length > 0 then
        match ast with
        | MethodDefInCtxt ctxt "init" (Some (body,_,nameRng)) ->
            match body with
            | BeginsWith(Call(CallExpr(BinOpExpr(OpMethInd, NameExpr("self",_),String("init_super",_),_),_,_),_))
            | Call(CallExpr(BinOpExpr(OpMethInd, NameExpr("self",_),NameExpr("init_super",_),_),_,_),_) ->
                ()
            | _ ->
                addError 
                    filePath
                    ("constructor does not call self:init_super on first line")
                    nameRng
        | _ ->
            ()

[<Export(typeof<LuaAnalyzer.Plugins.ITypeCollectorPlugin>)>]
type Initializer() =
    interface LuaAnalyzer.Plugins.ITypeCollectorPlugin with
        member self.Init () =
            addTypeCollector {
                name = "Basic Class System"
                typeGraphBuilder = typeGraphBuilder
                typeGraphIsTree = true
                externalTypeBuilder = externalTypeBuilder
                internalTypeBuilder = internalTypeBuilder
                decorate = Decorator.decorate
                detectMiscErrors = detectMiscErrors
            }

        member self.GetLibPaths () =
            seq [
                "basic_class_system\\class.lua"
            ]

        member self.GetName () =
            "Basic Class System"