﻿module Decorator

open LuaAnalyzer.Context
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.Type
open LuaAnalyzer.Operators

type GenList<'a> = System.Collections.Generic.List<'a>

/// tenv - the complete type environment of the project
/// modname - the name of the module being decorated
/// parentPrivTy - the private type of the parent (if this class has a parent)
/// parentName - the name of the parent (if the class has a parent)
/// 
/// returns the type to give to the self paramter of the init function
let getConstructorSelfTy (tenv : TypeEnvironment)
                         (modname : string)
                         (parentPrivTy : Option<Type>)
                         (parentName : Option<string>) 
                         : Type 
                         =

    let typeMap = tenv.typeMap
    let privateTy = (typeMap.Item (modname + "|private"))

    let privateConsTy =
        match parentPrivTy with
        | Some( RecordTy(_,_,_,_,Closed,_,parentFields,_,parentMethods,[],_) ) ->
            let parentInit = 
                match tenv.consMap.TryFind (parentName.Value) with
                | Some( CallableTy(desc,formals,varPars,rets,varRets,_,latent,defLocation) ) ->
                    {
                        desc = desc
                        ty = FunctionTy(desc,formals,varPars,[],None,true,latent,defLocation)
                        isAbstract = false
                        loc = defLocation
                    }
                | Some (UnknownTy(_)) ->
                    {
                        desc = ""
                        ty = UnknownTy
                        isAbstract = false
                        loc = NoLocation
                    }
                | _ ->
                    failwith "parent constructor should be a function or have a call metamethod"

            match privateTy with
            | RecordTy(name,desc,isStructural,displayStructural,Closed,metaset,fields,[],methods,[],loc) ->
                let delegates = fields.Item "delegates"
                let events = fields.Item "events"
                let consFields = parentFields.Add("delegates",delegates).Add("events",events)
                RecordTy(name,desc,false,false,Open,metaset,consFields,[],methods.Add("init_super", parentInit),[],loc)
            | _ ->
                failwith "private self type should be a record type"
        | _ ->
            match privateTy with
            | RecordTy(name,desc,isStructural,displayStructural,Closed,metaset,fields,[],methods,[],loc) ->
                let delegates = fields.Item "delegates"
                let events = fields.Item "events"
                let consFields = Map.empty.Add("delegates",delegates).Add("events",events)
                RecordTy(name,desc,isStructural,displayStructural,Open,metaset,consFields,[],methods,[],loc)
            | _ ->
                failwith "private self type should be a record type"

    privateConsTy 

let decorate (ctxt : Context) (modname : string) (ast : TypedStatement) =
    let tenv = ctxt.tenv
    let typeMap = tenv.typeMap

    let className = 
        match getName "" ast with
        | Some(className,_,_) ->
            className
        | None ->
            failwith "unreachable"

    let parentName, parentTy, parentPrivTy = 
        match (tenv.edges.Item modname).Length with
        | 0 ->
            None, None,None
        | _ ->
            let parentName = (tenv.edges.Item modname).[0]
            Some(parentName), Some(typeMap.Item parentName), Some (typeMap.Item (parentName+"|private"))

    let rec decRec (ast : TypedStatement) (reqMap : Map<string,string>) =
        match ast with
        | Sequence(ann,stats,rng) when stats.Count > 0 ->
            let reqMap = ref reqMap
            let decStats = new GenList<TypedStatement>(stats.Count)
            for i in 0..(stats.Count-1) do
                let s = stats.[i]
                decStats.Add( (decRec s (!reqMap)) )
                reqMap := 
                    match s with
                    | LocalAssign(
                        _,
                        [NameExpr(name,rng)],
                        [CallExpr(NameExpr("require",_),String(modName,_) :: _,_)],
                        _
                     ) ->
                        (!reqMap).Add(name,modName)
                    | _ ->
                        !reqMap

            Sequence(ann,decStats, rng)
        | LocalAssign(
            annotation,
            [NameExpr(className,classNameRng)],
            [CallExpr(BinOpExpr(OpMethInd, NameExpr("class",classRng), String("inherit",inheritRng),indRng),[NameExpr(parentName,parentRng)],exprsRng)],
            rng
            ) when reqMap.ContainsKey parentName ->

            let consMeta, consLoc = 
                match tenv.consMap.TryFind modname with
                | Some( RecordTy(_,_,_,_,Closed,meta,_,_,_,_,loc) ) ->
                    meta, loc
                | _ -> 
                    MetamethodSet.empty, NoLocation

            let eventsTy = RecordTy(
                "events",
                "the events published by the class being defined",
                true,
                false,
                Open,
                MetamethodSet.empty,
                Map.empty,
                [],
                Map.empty,
                [],
                consLoc
            )

            let eventsField = {
                desc = "published events"
                ty = eventsTy
                loc = consLoc
                dependencies = Set.empty
                isVisible = true
                isConst = true
            }

            let newClassTy = RecordTy(
                "newclass",
                "a class in the process of being defined",
                true,
                false,
                Open,
                { MetamethodSet.empty with Call = consMeta.Call },
                Map.add "events" eventsField Map.empty,
                [],
                Map.empty,
                [],
                consLoc
            )

            let callExpr = CallExpr(
                BinOpExpr(
                    OpMethInd, 
                    NameExpr("class",classRng), 
                    String("inherit",inheritRng),
                    indRng
                ),
                [NameExpr(parentName,parentRng)],
                exprsRng
            )

            let ascriptedCallExpr = Ascription(callExpr,newClassTy,indRng)

            LocalAssign(
                annotation,
                [NameExpr(className,classNameRng)],
                [ascriptedCallExpr],
                rng
            )
        | LocalAssign(
            annotation,
            [NameExpr(className,classNameRng)],
            [CallExpr(BinOpExpr(OpMethInd, NameExpr("class",classRng), String("new",newRng),indRng),[],exprsRng)],
            rng
            ) ->

            let consMeta, consLoc = 
                match tenv.consMap.TryFind modname with
                | Some( RecordTy(_,_,_,_,Closed,meta,_,_,_,_,loc) ) ->
                    meta, loc
                | _ -> 
                    MetamethodSet.empty, NoLocation

            let eventsTy = RecordTy(
                "events",
                "the events published by the class being defined",
                true,
                false,
                Open,
                MetamethodSet.empty,
                Map.empty,
                [],
                Map.empty,
                [],
                consLoc
            )

            let eventsField = {
                desc = "published events"
                ty = eventsTy
                dependencies = Set.empty
                loc = consLoc
                isVisible = true
                isConst = true
            }

            let newClassTy = RecordTy(
                "newclass",
                "a class in the process of being defined",
                true,
                false,
                Open,
                { MetamethodSet.empty with Call = consMeta.Call },
                Map.add "events" eventsField Map.empty,
                [],
                Map.empty,
                [],
                consLoc
            )

            let callExpr = CallExpr(
                BinOpExpr(
                    OpMethInd, 
                    NameExpr("class",classRng), 
                    String("new",newRng),
                    indRng
                ),
                [],
                exprsRng
            )

            let ascriptedCallExpr = Ascription(callExpr,newClassTy,indRng)

            LocalAssign(
                annotation,
                [NameExpr(className,classNameRng)],
                [ascriptedCallExpr],
                rng
            )

        | Assign(
            annotation,
            [BinOpExpr(OpSelect,NameExpr(x,rcl),String(methName,rmth),rind)], 
            [Function(desc,funcName,latent,self :: restFormals,varPars,rets,varRets,body,funRng)],
            rng
            ) when x = className ->

            let parentMethod = 
                match parentTy with
                | Some( RecordTy(_,_,_,_,Closed,_,fields,[],methods,[],_) ) ->
                    methods.TryFind methName
                | _ ->
                    None
            
            let mapFormal (name,desc,ty) =
                match parentMethod with
                | Some{desc=_;ty=FunctionTy(_,pformals,pVarPars,prets,pVarRets,_,platent,_);isAbstract=_;loc=_} ->
                    match List.tryFind (fun (n,d,t) -> n = name) pformals with
                    | Some (n,d,t) ->
                        (n,d,t)
                    | None ->
                        (name,desc,ty)
                | _ ->
                    (name,desc,ty)
            
            let rets,varRets,latent =
                match parentMethod with
                | Some meth ->
                    match meth.ty with
                    | FunctionTy(_,pformals,_,prets,varRets,_,platent,_) ->
                        prets,varRets,platent || latent
                    | UnknownTy ->
                        rets,varRets,latent
                    | _ ->
                        failwith "unreachable"
                | _ ->
                    rets,varRets,latent

            let selfName,selfDesc,selfTy = self

            let self' = 
                if methName = "init" then 
                    let consSelfTy = getConstructorSelfTy ctxt.tenv
                                                          modname 
                                                          parentPrivTy 
                                                          parentName 
                                                      
                    selfName,selfDesc, consSelfTy
                else
                    selfName,selfDesc,(typeMap.Item (modname + "|private"))
            Assign(
                annotation,
                [BinOpExpr(OpSelect,NameExpr(x,rcl),String(methName,rmth),rind)],
                [Function(desc,funcName,latent,self' :: List.map mapFormal restFormals,varPars,rets,varRets,body,funRng)],
                rng
            )
        | x ->
            x

    decRec ast Map.empty