﻿namespace BcsModuleCreator
{
  partial class ModuleCreatorControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.descriptionTextBox = new System.Windows.Forms.RichTextBox();
      this.descriptionLabel = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBox2
      // 
      this.textBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
      this.textBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
      this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
      this.textBox2.Location = new System.Drawing.Point(68, 34);
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new System.Drawing.Size(234, 20);
      this.textBox2.TabIndex = 10;
      this.textBox2.Text = "*none*";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 37);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(63, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "Parent Path";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Class Path";
      // 
      // textBox1
      // 
      this.textBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
      this.textBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
      this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
      this.textBox1.Location = new System.Drawing.Point(68, 8);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(234, 20);
      this.textBox1.TabIndex = 7;
      // 
      // descriptionTextBox
      // 
      this.descriptionTextBox.ImeMode = System.Windows.Forms.ImeMode.On;
      this.descriptionTextBox.Location = new System.Drawing.Point(6, 80);
      this.descriptionTextBox.Name = "descriptionTextBox";
      this.descriptionTextBox.Size = new System.Drawing.Size(661, 440);
      this.descriptionTextBox.TabIndex = 11;
      this.descriptionTextBox.Text = "";
      // 
      // descriptionLabel
      // 
      this.descriptionLabel.AutoSize = true;
      this.descriptionLabel.Location = new System.Drawing.Point(3, 64);
      this.descriptionLabel.Name = "descriptionLabel";
      this.descriptionLabel.Size = new System.Drawing.Size(60, 13);
      this.descriptionLabel.TabIndex = 12;
      this.descriptionLabel.Text = "Description";
      // 
      // ModuleCreatorControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.descriptionLabel);
      this.Controls.Add(this.descriptionTextBox);
      this.Controls.Add(this.textBox2);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.textBox1);
      this.Name = "ModuleCreatorControl";
      this.Size = new System.Drawing.Size(672, 527);
      this.Load += new System.EventHandler(this.ModuleCreatorControl_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.RichTextBox descriptionTextBox;
    private System.Windows.Forms.Label descriptionLabel;
  }
}
