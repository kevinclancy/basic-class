﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LoveEdit;
using LuaAnalyzer;

namespace BcsModuleCreator
{
  public partial class ModuleCreatorControl : UserControl
  {
    private string[] _parentCompletionSource = null;

    public ModuleCreatorControl(List<string> parentCompletionSource)
    {
      InitializeComponent();
      _parentCompletionSource = parentCompletionSource.ToArray();
    }

    private void ModuleCreatorControl_Load(object sender, EventArgs e)
    {
      Studio studio = Studio.Get();
      Project project = studio.CurrentProject;

      if (_parentCompletionSource.Length < 800)
      {
        //TODO: this is very slow--can we optimize this? or do it asynchronously?

        // For the class name, we use all paths with lua files in them (i.e. we suggest namespaces, not modules)
        foreach (string path in Studio.Get().CurrentProject.GetModulePaths())
          textBox1.AutoCompleteCustomSource.Add(path + ".");

        // For the parent box, we use all modules.
        textBox2.AutoCompleteCustomSource.AddRange(_parentCompletionSource);
      }
    }

    /// <summary>
    /// Gets a sequence of fields, extracted from the GUI control, 
    /// used to instantiate a new module template for this specific module type.
    /// </summary>
    public string[] GetNewModuleArgs()
    {
      return new string[]{
        // classPath
        textBox1.Text,

        // parentPath
        textBox2.Text,

        // module description,
        descriptionTextBox.Text,
      };
    }
  }
}
