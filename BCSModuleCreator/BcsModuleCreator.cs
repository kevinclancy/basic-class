﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using LoveEdit;
using LuaAnalyzer;

namespace BcsModuleCreator
{
  [Export(typeof(LoveEdit.IModuleCreator))]
  public class BcsModuleCreator : LoveEdit.IModuleCreator
  {
    private ModuleCreatorControl _currentControl;
    
    public string GetName()
    {
      return "Basic Class System";
    }

    public Control GetControl()
    {
      _currentControl = new ModuleCreatorControl(GetAllBCSModules());
      return _currentControl;
    }

    public List<string> GetAllBCSModules()
    {
      return ProjectChecker.getModulesCollectedBy(Studio.Get().CurrentProject.GetModules(),"Basic Class System");
    }

    public bool CreateNewModule(string[] args)
    {
      Studio studio = Studio.Get();
      Project project = studio.CurrentProject;

      string classPath = args[0];
      string className = classPath.Split('.').Last();
      string moduleDescription = args[2];

      if (!project.IsValidModuleName(classPath))
      {
        MessageBox.Show(
            "Illegal class name. Class names may only consist of letters, digits, '_', and '.'. Substrings between '.'s must be non-empty."
            + "\nAlso, class may not be named 'init'.");
        return false;
      }

      string parentPath = args[1];
      string parentName = parentPath.Split('.').Last();

      string text;
      if (parentPath == "*none*" || parentPath == "")
      {
        text = @"
--[[
@module
*moduledescription*
]]


local class = require('basic_class_system.class')
local *classname* = class:new()

function *classname*:init()

end

return *classname*

";
      }
      else
      {
        text = @"
--[[
@module
*moduledescription*
]]


local class = require('basic_class_system.class')
local *parentname* = require('*parentpath*')
local *classname* = class:inherit(*parentname*)

function *classname*:init()
  self:init_super()
end

return *classname*

";

        if (!project.IsValidModuleName(parentPath))
        {
          MessageBox.Show("Illegal parent class name. Class names may only consist of letters, digits, '_', and '.'. Substrings between '.'s must be non-empty.");
          return false;
        }

        if (!GetAllBCSModules().Contains(args[1]))
        {
          MessageBox.Show("Module '" + parentPath + "' does not exist or is not a Basic Class System module.");
          return false;
        }
      }

      text = text.Replace("*classname*", className);
      text = text.Replace("*parentname*", parentName);
      text = text.Replace("*parentpath*", parentPath);
      text = text.Replace("*moduledescription*", moduleDescription);

      //Remove leading newline
      text = text.Trim();

      string fileName = project.toFileName(classPath);
      if (System.IO.File.Exists(fileName))
      {
        MessageBox.Show("A module with name " + classPath + " already exists.");
        return false;
      }

      studio.CreateNewFile(fileName, text);
      return true;
    }

    public bool CreateNewModuleFromControl()
    {
      // Contract.Requires(_currentControl != null);
      var args = _currentControl.GetNewModuleArgs();
      return CreateNewModule(args);
    }

    public bool CreateNewModuleFromArgs(string[] args)
    {
      return CreateNewModule(args);
    }
  }
}
